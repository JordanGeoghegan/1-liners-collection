# SORT TEXT FILE ALPHABETICALLY (IN PLACE)
perl -e '$, = "\n"; print sort map { chomp; $_ } <>' input.txt

# EMULATE GREP
perl -ne '/STRING/ && print' *bank*.csv

# EMULATE uniq
perl -ne 'print unless $a{$_}++'

# Emulate "wc -l"
perl -pe '}{$_=$.' mylist.txt

# STRIP ^M CARRIAGE RETURNS FROM FILE
perl -p -i -e 's/\r\n$/\n/g' MYFILE.txt

# PRINT LOCAL TIME FROM INTEGER (SECONDS FROM UNIX EPOCH)
perl -le 'print scalar localtime 1483595082;'

# CONVERT LIST OF APPLE TIMES TO UNIX TIME (32 BIT TIME INTEGER)
perl -lne 'print 978307200 + $_ / 1000000000;'  < appletime.csv > unixtime.csv

# CONVERT LIST OF APPLE TIMES TO UNIX TIME (64 BIT TIME INTEGER)
perl -lne 'print 978307200 + $_;'  < appletime.csv > unixtime.csv

# CONVERT UNIX TIME TO RFC STANDARD TIME
perl -lne 'print scalar localtime $_;' < unixtime.txt 

# Batch Rename With Regex
for f in *.jpg; do mv "$f" "`echo $f | sed s/input/output/`"; done

# Add file extension to all files in current directory
for f in *; do mv "$f" "$f.mov"; done

# Print newline (or arbitrary string) above regex match
perl -plne 'print "" if(/STRING/);' 

# AFTER FIRST OCCURRENCE OF ‘REGEX’, DELETE REST OF LINE
perl -i -pe 's/(?<=REGEX).*//' /tmp/myfile.txt

# FIND AND REPLACE
perl -pe 's/INPUT/OUTPUT/g'

# STRIP INITIAL WHITESPACE FROM ALL LINES
perl -pe 's/^\s+//'

# STRIP TRAILING WHITESPACE FROM ALL LINES
perl -pe 's/ +$//' file 
sed 's/ *$//'
perl -lpe 's/\s*$//' (possibly broken [doesn’t output same hash as above methods])

# CAPITILIZE FIRST LETTER IN EVERY WORD
perl -p -e "@ph=map {ucfirst(lc)} split(/[\s.,-]+/);print qq(@ph)" file

# APPEND TO END OF EACH LINE
sed 's/$/ STRING/'

# PREPPEND TO BEGINNING OF EACH LINE
sed -e 's/^/STRING/' 

# REPLACE "FOO" WITH "BAR" IN ALL LINES NOT CONTAINING "STRING"
sed '/STRING/! s/FOO/BAR/' file

# PERFORM ARITHMETIC WITH ARBITRARY ACCURACY LEVELS 
# (increase ‘scale’ amount for added accuracy { echo may be more portable})
echo 'scale=10; 22 / 3' | bc

# REMOVE WHITESPACE GREATER THAN 2 LINES
awk '!NF {if (++n <= 2) print; next}; {n=0;print}'

# FFMPEG Downmix 5.1 to “Nightmode” Stereo Audio
# Prevents dialogue from being super quiet, and action scenes from being super loud
# In short, does some clever dynamic range compression tricks to prevent ear bleeding
# “The Nightmode Dialogue formula, created by Robert Collier on the Doom9 forum”
# See "https://forum.doom9.org/showthread.php?t=168267"
ffmpeg -i "input.mp4" -vcodec copy -c:a ac3 -af "pan=stereo|FL=FC+0.30*FL+0.30*BL|FR=FC+0.30*FR+0.30*BR" "/tmp/stereo.mp4"

ffmpeg -i "input.mp4" -c:v libx264 -c:a ac3 -crf 24 -af "pan=stereo|FL=FC+0.30*FL+0.30*BL|FR=FC+0.30*FR+0.30*BR" "/tmp/stereo.mp4"

# EXTRACT MP3 FROM YOUTUBE VIDEO
youtube-dl --extract-audio --audio-format mp3 -o "%(title)s.%(ext)s" https://www.youtube.com/playlist?list=PL2F81DD0F2DBF4ABA

# PULL SECTION OF VIDEO FROM FILE
ffmpeg -ss 00:00:30 -i orginalfile -t 00:00:05 -vcodec copy -acodec copy newfile

# COMBINE IMAGE AND AUDIO TO MAKE VIDEO
ffmpeg -loop 1 -i image.jpg -i audio.wav -c:v libx264 -tune stillimage -c:a aac -b:a 192k -pix_fmt yuv420p -shortest out.mp4

# Extract and convert video to jpeg frame by frame
ffmpeg -i video.webm thumb%04d.jpg -hide_banner

# VI DELETE ALL LINES CONTAINING “STRING”
:g /STRING/d

# VI DELETE ALL LINES _NOT_ CONTAINING “STRING”
:%g!/STRING/d

# GREP FOR STRING WHILE MAINTAINING WHITE SPACE (USE AWK TO DELETE WHITESPACE GREATER THAN 2 LINES)
egrep "PortableApps|^$" fdupesGeo.txt | awk '!NF {if (++n <= 2) print; next}; {n=0;print}' 

# DELETE LINES WITH BLANK LINES ABOVE AND BELOW (DELETE LONE LINES [fdupes])
awk '$2' ORS='\n\n' FS='\n' RS= myfile.txt

# Print file, exluding first 363 lines
tail +364 myfile.txt
awk 'NR>363' my file.txt

### Disk Cloning

# Clone local disk with dd, compress with gzip and send file over ssh to remote machine
dd if=/dev/sda | gzip -1 - | ssh user@remote dd of=image.gz

# Clone disk on remote machine, compress with gzip and retrieve over ssh to local machine
ssh user@remote "dd if=/dev/sda | gzip -1 -" | dd of=image.gz

# Restore disk clone over ssh to remote machine, doing gunzip on remote server (saves bandwidth over the wire)
dd if=diskclone.img.gz bs=1M | ssh -c aes128-ctr user@remote 'gunzip -c | doas dd of=/dev/rsd0c bs=1M’

# Restore disk clone over ssh to remote machine, doing gunzip on local machine before sending over wire
dd if=diskclone.img.gz bs=1M | gunzip -c | ssh -c aes128-ctr user@remote | doas dd of=/dev/rsd0c bs=1M
